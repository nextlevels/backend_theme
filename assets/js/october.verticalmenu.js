/*
 * Creates a vertical responsive menu.
 *
 * JavaScript API:
 * $('#menu').verticalMenu()
 *
 * Dependences:
 * - Drag Scroll (october.dragscroll.js)
 */
+function ($) { "use strict";

    var VerticalMenu = function (element, toggle, options) {
        this.$el = $(element)
        this.body = $('body')
        this.toggle = $(toggle)
        this.options = options || {}
        this.options = $.extend({}, VerticalMenu.DEFAULTS, this.options)
        this.wrapper = $(this.options.contentWrapper)
        this.breakpoint = options.breakpoint

        if (this.body.find('.mainmenu-collapsed').length > 0) {
            return;
        }

        var self = this

        /*
         * Insert the menu
         */
        this.menuPanel = $('<div></div>').appendTo('body').addClass(this.options.collapsedMenuClass).css('width', self.options.menuWidth)
        this.menuContainer = $('<div></div>').appendTo(this.menuPanel).css('display', 'block')
        this.menuElement = this.$el.clone().appendTo(this.menuContainer).css('width', 'auto')



        /*
         * Handle the menu toggle click
         */
        this.toggle.click(function() {
            if (!self.body.hasClass(self.options.bodyMenuOpenClass)) {
                var wrapperWidth = self.wrapper.outerWidth()

                self.menuElement.dragScroll('goToStart')

                self.wrapper.css({
                    'position': 'absolute',
                    'min-width': self.wrapper.width(),
                    'height': '100%'
                })
                self.body.addClass(self.options.bodyMenuOpenClass)
                self.menuContainer.css('display', 'block')

                self.wrapper.animate({'left': self.options.menuWidth}, { duration: 200, queue: false })
                self.menuPanel.animate({'width': self.options.menuWidth}, {
                    duration: 200,
                    queue: false,
                    complete: function() {
                        self.menuElement.css('width', self.options.menuWidth)
                    }
                })
            }

            return false
        })

        /*
         * Make the menu draggable
         */
        this.menuElement.dragScroll({
            vertical: true,
            useNative: true,
            start: function(){self.menuElement.addClass('drag')},
            stop: function(){self.menuElement.removeClass('drag')},
            scrollClassContainer: self.menuPanel,
            scrollMarkerContainer: self.menuContainer
        })

        this.menuElement.on('click', function() {
            // Do not handle menu item clicks while dragging
            if (self.menuElement.hasClass('drag'))
                return false
        })
    }

    VerticalMenu.DEFAULTS = {
        menuWidth: 230,
        breakpoint: 769,
        bodyMenuOpenClass: 'mainmenu-open',
        collapsedMenuClass: 'mainmenu-collapsed',
        contentWrapper: '#layout-canvas'
    }

    // VERTICAL MENU PLUGIN DEFINITION
    // ============================

    var old = $.fn.verticalMenu

    $.fn.verticalMenu = function (toggleSelector, option) {
        return this.each(function () {
            var $this = $(this)
            var data  = $this.data('oc.verticalMenu')
            var options = typeof option == 'object' && option

            if (!data) $this.data('oc.verticalMenu', (data = new VerticalMenu(this, toggleSelector, options)))
            if (typeof option == 'string') data[option].call($this)
        })
    }

    $.fn.verticalMenu.Constructor = VerticalMenu

    // VERTICAL MENU NO CONFLICT
    // =================

    $.fn.verticalMenu.noConflict = function () {
        $.fn.verticalMenu = old
        return this
    }

}(window.jQuery);
