/*
--------------------------------------------------------------
  Template Name: Orbiter - Responsive Admin Dashboard Template
  File: Core JS File
--------------------------------------------------------------
 */
"use strict";

$(document).ready(function() {

    /* -- Menu js -- */
    jQuery.sidebarMenu($('.vertical-menu'));
    $(function() {
        for (var a = window.location, abc = $(".vertical-menu a").filter(function() {
            return this.href == a;
        }).addClass("active").parent().addClass("active"); ;) {
            if (!abc.is("li")) break;
            abc = abc.parent().addClass("in").parent().addClass("active");
        }
    });
    /* -- Infobar Setting Sidebar -- */
    $("#infobar-settings-open").on("click", function(e) {
        e.preventDefault();
        // $(".infobar-settings-sidebar-overlay").css({"background": "rgba(0,0,0,0.4)", "position": "fixed"});
        $("#infobar-settings-sidebar").addClass("sidebarshow");
    });

    $("#infobar-settings-close").on("click", function(e) {
        e.preventDefault();
        //  $(".infobar-settings-sidebar-overlay").css({"background": "transparent", "position": "initial"});
        $("#infobar-settings-sidebar").removeClass("sidebarshow");
    });


    $("#infobar-help-sidebar-open").on("click", function(e) {
        e.preventDefault();
        $(".infobar-settings-sidebar-overlay").css({"background": "rgba(0,0,0,0.4)", "position": "fixed"});
        $("#infobar-help-sidebar").addClass("sidebarshow");
    });

    $("#infobar-help-close").on("click", function(e) {
        e.preventDefault();
        $(".infobar-settings-sidebar-overlay").css({"background": "transparent", "position": "initial"});
        $("#infobar-help-sidebar").removeClass("sidebarshow");
    });

    $("#example-images-sidebar-open").on("click", function(e) {
        e.preventDefault();
        $("#example-images-sidebar").addClass("sidebarshow");

        $(".menu-hamburger")[0].click();
    });

    $("#example-images-close").on("click", function(e) {
        e.preventDefault();
        $("#example-images-sidebar").removeClass("sidebarshow");

        $(".menu-hamburger")[0].click();
    });


    /* -- Menu Hamburger -- */
    $(".menu-hamburger").on("click", function(e) {
        e.preventDefault();
        $("body").toggleClass("toggle-menu");
        $(".menu-hamburger img").toggle();
    });
    /* -- Menu Topbar Hamburger -- */
    $(".topbar-toggle-hamburger").on("click", function(e) {
        e.preventDefault();
        $("body").toggleClass("topbar-toggle-menu");
        $(".topbar-toggle-hamburger img").toggle();
    });
    /* -- Menu Scrollbar -- */
    /*$('.vertical-menu').slimscroll({
        height: '700px',
        position: 'right',
        size: "7px",
        color: '#CFD8DC',
    });*/
    /* -- Media Size -- */
    function mediaSize() {
        if (window.matchMedia('(max-width: 767px)').matches) {
            $("body").removeClass("toggle-menu");
            $(".menu-hamburger img.menu-hamburger-close").hide();
            $(".menu-hamburger img.menu-hamburger-collapse").show();
        }
    };
    mediaSize();
    window.addEventListener('resize', mediaSize, false);

    /* -- Bootstrap Popover -- */
    $('[data-toggle="popover"]').popover();
    /* -- Bootstrap Tooltip -- */
    $('[data-toggle="tooltip"]').tooltip();


    $("#save-top").on("click", function(e) {
        $(".form-buttons .loading-indicator-container button:nth-child(1)").click();
    });

    $("#saveandclose-top").on("click", function(e) {
        $(".form-buttons .loading-indicator-container button:nth-child(2)").click();
    });

    $('.help-sidebar-wrapper figure[data-video]').filter(
        function(){
            return ($(this).attr('data-video').length > 0);
        }).each(function(index) {

        var video = $('<video />', {
            id: 'video'+index,
            src: $(this).attr('data-video'),
            type: 'video/mp4',
            controls: true
        });
        video.css('width','100%');
        $( this ).append(video);
    });

    var imgURL = "";

    $("a, img").on("mousedown", function(e) {
        var $target = $(e.target);
        if ($target.prop("tagName").toLowerCase() == 'img') {
            imgURL = $target.attr('data-src');
            console.log(imgURL);
        }
    });

    $(".upload-object").on("dragenter dragover dragleave", function(e) {
        // need for proper ondrop event activation
        e.preventDefault();
        e.stopPropagation();
    }).on("drop", function(e) {
        e.preventDefault();
        e.stopPropagation();
        console.debug("Dropped! URL:", imgURL);
    });
});
